"use strict";

const EVENT_INIT = "timer.init";
const EVENT_START = "timer.start";
const EVENT_STOP = "timer.stop";

class BaseModel {
  data = {};

  get(name) {
    return this.data[name];
  }

  set(name, value) {
    if (value !== this.data[name]) {
      this.data[name] = value;
    }
  }

  change(name, prev, next) {}
}

class Time {
  constructor(hours, minutes, seconds) {
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
  }

  toString() {
    return [this.hours, this.minutes, this.seconds].join(":");
  }
}

class UIModel extends BaseModel {}

class UIController {
  constructor() {
    // this.model = new UIModel();
    // this.model.set("time", null);
    this.state = {
      time: Time(0, 0, 0),
      isRunning: false,
    };

    this.model.this.el = {
      timer: document.querySelector(".ui-timer"),
      playStopButton: document.querySelector(".ui-button.play"),
    };
  }

  static onTimerStart(e) {}

  static onTimerStop(e) {}

  run() {
    document.addEventListener(EVENT_START, this.onTimerStart);
    document.addEventListener(EVENT_STOP, this.onTimerStop);

    this.el.timer.innerText = "00:00:00";

    this.el.playStopButton.addEventListener("click", function (e) {
      console.log(e.currentTarget);
      // this.removeClass()
    });
  }
}
