const WORK_PERIOD = 25 * 60;

(function () {
  const timerValue = document.querySelector(".ui-timer");
  const playButton = document.querySelector(".ui-button.play");
  const history = document.querySelector("table.ui-history > tbody");

  let interval;
  let current = 0;
  const records = [];

  function leftPad(number, targetLength) {
    var output = number + "";
    while (output.length < targetLength) {
      output = "0" + output;
    }
    return output;
  }

  function formatInterval(value) {
    value = value || current;
    const hours = leftPad(Math.floor(current / 3600), 2);
    const minutes = leftPad(Math.floor((current % 3600) / 60), 2);
    const seconds = leftPad(current % 60, 2);
    return `${hours}:${minutes}:${seconds}`;
  }

  function setValue() {
    timerValue.innerText = formatInterval();
  }

  function addRecord() {
    const end = new Date();
    const start = new Date(end - current * 1000);
    records.push([start, end]);
    const row = document.createElement("tr");
    row.innerHTML = `
        <td>${start.toLocaleTimeString()}</td>
        <td>${end.toLocaleTimeString()}</td>
        <td>${formatInterval()}</td>
    `;
    history.prepend(row);
  }

  function startTimer() {
    // start timer
    interval = setInterval(function () {
      current += 1;
      setValue();
    }, 1000);
  }

  function stopTimer() {
    // stop timer
    clearInterval(interval);
    interval = null;
    addRecord();
    current = 0;
    setValue();
  }

  function onPlayButtonClick(e) {
    playButton.classList.toggle("play");
    playButton.classList.toggle("stop");

    if (interval) {
      stopTimer();
    } else {
      startTimer();
    }
  }
  
  playButton.addEventListener("click", onPlayButtonClick);
  
  setValue();
  
})();
